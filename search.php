<?php /* Template Name: cercador */ ?>
<?php get_header();?>



    <div class="titol_post" style="background-image: url('<?php

    echo get_theme_file_uri('img/fons-general.jpeg');
    
    ?>
    ');" />
      <div class="titol-post-text">
<h1><?php pll_e('Resultats de cerca');?>
      </div>
    </div>


<div id="principal">

  <div class="post-text-principal">

  <?php if ( have_posts() ): ?>
                <?php while( have_posts() ): ?>
                    <?php the_post(); ?>
                    <div class="search-result">
                        <h2><?php the_title(); ?></h2>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" class="read-more-link">
                            <?php pll_e('Llegeix més') ?>
                        </a>
                    </div>
                <?php endwhile; ?>
                <?php the_posts_pagination(); ?>
            <?php else: ?>
                <p><?php pll_e('No hi ha resultats de cerca');?></p>
            <?php endif; ?>  

</div>
<div class="post-columna-dreta">
<?php dynamic_sidebar( 'lateral-interiors' );?>

</div>



</div>

<?php get_footer();?>
