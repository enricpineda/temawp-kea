<?php get_header();?>


<?php
if ( have_posts() ) :
    while ( have_posts() ) : the_post();
    ?>
    


<div id="principal">
  <!--
<div class="portada-publicacio">

</div>
-->
  <div class="post-text-principal">
  <h1 style="color: #00871d;"><?php the_title();?></h1>
<?php the_content();?>
<p><?php $categories = get_the_category();

$categoria = $categories[0];
//echo $categoria['slug'];
$lang = pll_current_language('slug');
?></p>
</div>
<div class="post-columna-dreta">
<?php the_post_thumbnail( 'full',['class' => 'imatge-responsive'] );?>
<p>
<?php if ($lang == "ca") {$url_butiko = "url_butiko_cat";} else {$url_butiko = "url_butiko_eo";}
$adreca_butiko = get_field($url_butiko);
if ($adreca_butiko != "") {
  ?>
  <a href="<?php echo $adreca_butiko;?>" target="_blank" class="link-butiko"><?php pll_e("Compra el llibre a la Butiko KEA");?></a>

  <?php
}
?>
</p>
  <?php
  if (esRevista($categoria->term_id) === true) {
    if (post_password_required() === false) {
  $fitxer = get_field('arxiu_descarrega');
  if ($fitxer) {
    $adreca_fitxer = $fitxer['url'];
  ?>
  <h3><?php pll_e("Descàrrega");?></h3>
  <p><a href="<?php echo esc_attr($adreca_fitxer);?>"><?php pll_e("Descarrega't aquest número");?></a></p>
<?php }} ?>
<h3><?php pll_e("Números publicats");?></h3>
<?php if ($lang == "ca") {$urlinici = "/publicacions-kea/";} else {$urlinici = "/eldonoj/";} ?>
<p><a href="<?php echo $urlinici.$categoria->slug."/";?>"><?php pll_e("Accedeix a l'arxiu");?></a></p>

<?php
} 
?>

</div>



</div>
<?php
endwhile;
endif;
?>
<?php get_footer();?>