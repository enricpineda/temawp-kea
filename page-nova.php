<?php /* Template Name: Nova interior KEA */ ?>
<?php get_header();?>


<?php
if ( have_posts() ) :
    while ( have_posts() ) : the_post();
    ?>
    


<div id="principal">

  <div class="post-text-principal">
  <h1 style="color:#00871d;"><?php the_title();?></h1>
  <?php the_content();?></div>
<div class="post-columna-dreta">
<?php dynamic_sidebar( 'lateral-interiors' );?>

</div>



</div>
<?php
endwhile;
endif;
?>
<?php get_footer();?>
